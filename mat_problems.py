# version code 80e56511a793+
# Please fill out this stencil and submit using the provided submission script.

from mat import Mat
from vec import Vec



## 1: (Problem 4.17.1) Computing matrix-vector products
# Please represent your solution vectors as lists.
vector_matrix_product_1 = [.5 -.5]
#vector_matrix_product_2 = ...
#vector_matrix_product_3 = ...



## 2: (Problem 4.17.2) Matrix-vector multiplication to swap entries
# Represent your solution as a list of rowlists.
# For example, the 2x2 identity matrix would be [[1,0],[0,1]].

#M_swap_two_vector = ...



## 3: (Problem 4.17.3) [z+x, y, x] Matrix-vector multiplication
#three_by_three_matrix = ... # Represent with a list of rowlists.



